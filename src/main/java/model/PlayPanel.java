package main.java.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings("unused")
public class PlayPanel extends JPanel implements ActionListener, KeyListener {

	private static final long serialVersionUID = -2793076059738635270L;

	private PlayFrame playWindow;

	private PlayerBar playerBar;

	private CpuBar cpuBar;

	private Timer timer;

	private PlayerBall playerBall;

	private String backgroundPath;

	private BufferedImage gameImage;

	private ReentrantLock ballLock;

	private ReentrantLock playerBarLock;

	private int playerScore = 0;

	private int cpuScore = 0;

	private int level = 1;

	private Random random;

	public PlayPanel(PlayFrame playWindow) {
		this.playWindow = playWindow;
		this.backgroundPath = "wood.jpg";
		this.random = new Random();
		this.ballLock = new ReentrantLock();
		this.playerBarLock = new ReentrantLock();
		this.timer = new Timer(10, this);
		this.addKeyListener(this);
		this.setFocusable(true);
		this.playerBar = new PlayerBar(Color.WHITE, this, playWindow.getWidth() / 2 - PlayerBar.WIDTH / 2,
				playWindow.getHeight() - (PlayerBar.HEIGHT * 5 - 3), playerBarLock);
		this.cpuBar = new CpuBar(Color.MAGENTA, this, playWindow.getWidth() / 2 - PlayerBar.WIDTH / 2,
				playWindow.getHeight() - (PlayerBar.HEIGHT * 15 - 3), ballLock, playerBarLock);
		this.playerBall = new PlayerBall(Color.WHITE, this,
				random.nextInt(playWindow.getWidth() - PlayerBall.WIDTH * 2) + PlayerBall.WIDTH, PlayerBall.HEIGHT,
				ballLock);

	}

	@Override
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);

		try {
			gameImage = ImageIO.read(PlayFrame.class.getClassLoader().getResourceAsStream(backgroundPath));
			g.drawImage(gameImage, 0, 0, this);
		} catch (IOException e) {
			gameImage = null;
		}
		this.cpuBar.paint(g);
		this.playerBar.paint(g);
		this.playerBall.paint(g);
		g.setColor(Color.WHITE);
		g.drawString("Puntuación Jugador: " + playerScore, 0, 10);
		g.drawString("Puntuación CPU: " + cpuScore, 0, 25);
		g.drawString("Nivel: " + level, 0, 40);

	}

	public PlayerBar getPlayerBar() {
		return playerBar;
	}

	public CpuBar getCpuBar() {
		return cpuBar;
	}

	public PlayerBall getPlayerBall() {
		return playerBall;
	}

	public void playerLost() {

		// Detén el timer y muestra el diálogo para saber si se continúa jugando
		playerBall.stopRefresh();
		cpuBar.stopRefresh();
		
		this.timer.stop();
		
		this.playerBall.interrupt();
		this.cpuBar.interrupt();
		
		boolean playerWon = playerScore > cpuScore;
		
		String outcome =  playerWon ? "Victoria!" : "Derrota";
		
		int answer = JOptionPane.showConfirmDialog(this, outcome +", puntuación: " + playerScore + "\n¿Jugar de nuevo?",
				"Pong", JOptionPane.YES_NO_OPTION);
		if (answer == JOptionPane.YES_OPTION) {
			resetGame();
		} else {
			playWindow.dispose();
		}

	}

	public void resetGame() {
		
		this.playerScore = 0;
		this.cpuScore = 0;
		this.level = 1;
		this.playerBar = new PlayerBar(Color.WHITE, this, playWindow.getWidth() / 2 - PlayerBar.WIDTH / 2,
				playWindow.getHeight() - (PlayerBar.HEIGHT * 5 - 3), playerBarLock);
		this.cpuBar = new CpuBar(Color.MAGENTA, this, playWindow.getWidth() / 2 - PlayerBar.WIDTH / 2,
				playWindow.getHeight() - (PlayerBar.HEIGHT * 15 - 3), ballLock, playerBarLock);
		this.playerBall = new PlayerBall(Color.WHITE, this,
				random.nextInt(playWindow.getWidth() - PlayerBall.WIDTH * 2) + PlayerBall.WIDTH, PlayerBall.HEIGHT,
				ballLock);
		this.playWindow.playMusicOfLevel(1);
		this.adjustLevel();
		this.startGame();
		
	}

	public void ballBouncedFromPlayerBar() {
		cpuBar.pushTheTempo();
		adjustLevel();
		playerScore++;
	}

	public void ballBouncedFromCpuBar() {
		cpuBar.pushTheTempo();
		adjustLevel();
		cpuScore++;
	}

	// Método para iniciar los componentes con sus respectivos threads
	public void startGame() {

		int maxStartupTries = 3;
		int tries = 0;

		while (!this.isValid() && tries <= maxStartupTries) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if (this.isValid()) {
			timer.start();
			playerBall.start();
			cpuBar.start();
		} else {
			throw new RuntimeException("Error initializing main game panel");
		}
	}

	@Override
	public void keyPressed(KeyEvent event) {

		// Mandamos a la barra el código de la tecla presionada con una operación OR
		// para detectar si la tecla
		// control está presionada
		playerBar.keyPressed(event.getKeyCode() | event.getModifiersEx());
	}

	@Override
	public void keyReleased(KeyEvent event) {

	}

	@Override
	public void keyTyped(KeyEvent event) {

	}

	@Override
	public void actionPerformed(ActionEvent event) {

		// Solo actualizamos los objetos si el panel es válido
		if (this.isValid()) {
			this.repaint();
		}
	}

	private void adjustLevel() {

		if (playerScore >= 1 && playerScore < 2) {
			level = 1;
		} else if (playerScore >= 2 && playerScore < 4) {
			level = 2;
		} else if (playerScore >= 4 && playerScore < 6) {
			level = 3;
		} else if (playerScore >= 6 && playerScore < 8) {
			level = 4;
		} else if (playerScore >= 8 && playerScore < 10) {
			level = 5;
		} else if (playerScore == 10) {
			level = 6;
		} else if (playerScore == 11) {
			level = 7;
		} else if (playerScore >= 20 && playerScore < 30) {
			level = 8;
		} else if (playerScore >= 30 && playerScore < 50) {
			level = 10;
		}
	
		// The level must be between 0 and 10
		assert level > 0 && level <= 10 : "The level is out of the allowed range";

		playerBall.setDifficulty(level);
	}

}
