package main.java.model;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

public class PlayFrame extends JFrame {

	private static final long serialVersionUID = -4126537798692855533L;

	private final String STAGE_ONE_MUSIC = "stageone.aiff";
	private final String STAGE_TWO_MUSIC = "stagetwo.aiff";
	private final String STAGE_THREE_MUSIC = "stagethree.aiff";

	public static final int WIDTH = 400;
	public static final int HEIGHT = 600;

	private PlayPanel playPanel;

	private GameAudioPlayer gameAudioPlayer;

	public PlayFrame() {
		this.setSize(WIDTH, HEIGHT);
		this.setTitle("Pong Game");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.playPanel = new PlayPanel(this);
		this.add(this.playPanel);
		this.initializePlayer();
		this.playPanel.startGame();
		playMusicOfLevel(1);
	}

	private void initializePlayer() {
		
		JOptionPane.showMessageDialog(this, "Bienvenido al juego de pong!\n La raqueta morada es controlada por el CPU, la raqueta blanca por ti.\nEl movimiento es usando las flechas, puedes presionar control al mismo tiempo para moverte más rápido.");
		
		try {
			gameAudioPlayer = new GameAudioPlayer();
		} catch (Exception ex) {
			System.out.println("Error initializing player");
		}
	}

	public void playMusicOfLevel(int level) {

		try {
			switch (level) {
			case 1:
				gameAudioPlayer.switchTrack(STAGE_ONE_MUSIC);
				break;
			case 2:
				gameAudioPlayer.switchTrack(STAGE_TWO_MUSIC);
				break;
			case 3:
				gameAudioPlayer.switchTrack(STAGE_THREE_MUSIC);
				break;
			default:
				gameAudioPlayer.switchTrack(STAGE_ONE_MUSIC);
			}
		} catch (Exception e) {
			System.out.println("Error playing music of level: " + level);
		}
	}

	public void pauseMusic() {
		gameAudioPlayer.pause();
	}

	private class GameAudioPlayer {

		private Clip clip;
		private AudioInputStream audioInputStream = null;
		private String filePath;

		public GameAudioPlayer() {

		}

		public void switchTrack(String path)
				throws UnsupportedAudioFileException, IOException, LineUnavailableException {

			if (audioInputStream != null) {
				audioInputStream.close();
				clip.stop();
				clip.close();
			}

			this.clip = AudioSystem.getClip();
			this.filePath = path;

			InputStream is = PlayFrame.class.getClassLoader().getResourceAsStream(filePath);

			this.audioInputStream = AudioSystem.getAudioInputStream(new BufferedInputStream(is));

			this.clip.open(audioInputStream);
			this.clip.loop(Clip.LOOP_CONTINUOUSLY);
			start();
		}

		public void start() {
			clip.start();
		}

		public void pause() {
			clip.stop();
		}

	}
}
