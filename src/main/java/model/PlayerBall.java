package main.java.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.concurrent.locks.ReentrantLock;

class PlayerBall extends Thread {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1156109203272627517L;

	public static final int WIDTH = 13;
	public static final int HEIGHT = 13;

	private int x;
	private int y;

	private Color color;

	private PlayPanel playPanel;

	private ReentrantLock ballLock;

	private boolean active = true;

	private int ballSpeed = 1;

	private int xChange = 1;
	private int yChange = 1;

	public PlayerBall(Color color, PlayPanel playPanel, int x, int y, ReentrantLock ballLock) {
		this.ballLock = ballLock;
		this.color = color;
		this.playPanel = playPanel;
		this.x = x;
		this.y = y;
	}

	@Override
	public void run() {
		update();
	}
	
	public void stopRefresh() {
		active = false;
	}

	public void setDifficulty(int level) {
		assert level <= 10 && level > 0 : "Game level out of the allowed range";
		ballSpeed = level < 10 && level > 0 ? level : 1;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Rectangle getBallBounds() {
		return new Rectangle(x, y, WIDTH, HEIGHT);
	}

	public void paint(Graphics g) {
		g.setColor(color);
		g.fillOval(x, y, WIDTH, HEIGHT);
	}

	public void update() {

		while (active) {

			waitSomeTime();
			ballLock.lock();

			// La pelota rebota en las paredes verticales
			if (x + xChange <= 0 || x + xChange >= playPanel.getWidth() - WIDTH) {
				xChange = -xChange;
			}

			// La pelota rebota en el techo
			if (y + yChange <= 0) {
				yChange = -yChange;
			}

			// Movemos la pelota
			x += xChange;
			y += yChange;

			// Revisar si la pelota va a rebotar en la raqueta
			checkForCollisions();

			// Si la pelota toca el borde inferior el jugador pierde
			if (y + HEIGHT >= playPanel.getHeight()) {
				playPanel.playerLost();
			}

			ballLock.unlock();

			// La pelota no debe de salir del campo de juego
			assert y > 0 && y < playPanel.getHeight() : "Ball was outside of vertical range, y: " + y + " height: "
					+ playPanel.getHeight();
			// La barra no debe de salir del campo de juego
			assert x > 0 && x < playPanel.getWidth() : "Bar was outside of horizontal range, x: " + x + " width: "
					+ playPanel.getWidth();

		}

	}

	// Este método espera un poco de tiempo para no refrescar constantemente
	private void waitSomeTime() {

		long waitingTimeMilis = -1;
		int waitingTimeNanos = -1;

		switch (ballSpeed) {
		case 1:
			waitingTimeMilis = 10;
			waitingTimeNanos = 0;
			break;
		case 2:
			waitingTimeMilis = 9;
			waitingTimeNanos = 500000;
			break;
		case 3:
			waitingTimeMilis = 9;
			waitingTimeNanos = 0;
			break;
		case 4:
			waitingTimeMilis = 8;
			waitingTimeNanos = 500000;
			break;
		case 5:
			waitingTimeMilis = 8;
			waitingTimeNanos = 0;
			break;
		case 6:
			waitingTimeMilis = 7;
			waitingTimeNanos = 500000;
			break;
		case 7:
			waitingTimeMilis = 7;
			waitingTimeNanos = 0;
			break;
		case 8:
			waitingTimeMilis = 6;
			waitingTimeNanos = 0;
			break;
		case 9:
			waitingTimeMilis = 5;
			waitingTimeNanos = 0;
			break;
		case 10:
			waitingTimeMilis = 4;
			waitingTimeNanos = 0;
			break;
		default:
			waitingTimeMilis = 3;
			waitingTimeNanos = 0;
		}

		try {
			Thread.sleep(waitingTimeMilis, waitingTimeNanos);
		} catch (InterruptedException e) {
			throw new RuntimeException("Error sleeping ball refresh thread", e);
		}
	}

	private void checkForCollisions() {

		// Ver si hay intersecciones de la pelota con alguna de las barras
		if (playPanel.getPlayerBar().getBarBounds().intersects(getBallBounds())
				|| playPanel.getCpuBar().getBarBounds().intersects(getBallBounds())) {

			// Es con la barra del jugador
			if (playPanel.getPlayerBar().getBarBounds().intersects(getBallBounds())) {

				// Revisamos de qué lado tocó la raqueta
				boolean barTouchedLeft = x - WIDTH < playPanel.getPlayerBar().getX() + PlayerBar.WIDTH / 2;

				// Marcamos el rebote
				yChange = -yChange;

				// Si toca el lado izquierdo, rebota a la derecha y viceversa
				xChange = barTouchedLeft ? -xChange : xChange;

				// Rebotamos y subimos la puntuación
				y += yChange;
				playPanel.ballBouncedFromPlayerBar();

				// Revisamos que efectivamente ya no haya intersección, si todavía la hay,
				// subimos un poquito la bola hasta que ya no haya
				while (playPanel.getPlayerBar().getBarBounds().intersects(getBallBounds())) {
					y -= 1;
				}

				// Una vez que la bola rebota en la barra, ya no debe estar en ella
				assert !playPanel.getPlayerBar().getBarBounds()
						.intersects(getBallBounds()) : "Ball was still intersecting with player bar";

			} else {

				// Es con la barra del CPU
				// Revisamos de qué lado tocó la raqueta
				boolean barTouchedLeft = x - WIDTH < playPanel.getCpuBar().getX() + CpuBar.WIDTH / 2;

				// Marcamos el rebote
				yChange = -yChange;

				// Si toca el lado izquierdo, rebota a la derecha y viceversa
				xChange = barTouchedLeft ? -xChange : xChange;

				// Rebotamos y subimos la puntuación
				y += yChange;
				playPanel.ballBouncedFromCpuBar();

				// Revisamos que efectivamente ya no haya intersección, si todavía la hay,
				// subimos un poquito la bola hasta que ya no haya
				while (playPanel.getCpuBar().getBarBounds().intersects(getBallBounds())) {
					y -= 1;
				}

				// Una vez que la bola rebota en la barra, ya no debe estar en ella
				assert !playPanel.getCpuBar().getBarBounds()
						.intersects(getBallBounds()) : "Ball was still intersecting with cpu bar";

			}
		}
	}
}
