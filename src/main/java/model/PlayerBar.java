package main.java.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.concurrent.locks.ReentrantLock;

class PlayerBar {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1156109103272627517L;

	public static final int WIDTH = 50;
	public static final int HEIGHT = 9;

	private int x;
	private int y;

	private static final int SPEED_MODIFIER = InputEvent.CTRL_DOWN_MASK;

	private static final int MOVE_LEFT = KeyEvent.VK_LEFT;

	private static final int MOVE_RIGHT = KeyEvent.VK_RIGHT;

	private static final int MOVE_UP = KeyEvent.VK_UP;

	private static final int MOVE_DOWN = KeyEvent.VK_DOWN;

	private static final int SPEED_ONE = 18;

	private static final int SPEED_TWO = 50;

	private PlayPanel playPanel;

	private Color color;

	private ReentrantLock playerBarLock;

	public PlayerBar(Color color, PlayPanel playPanel, int x, int y, ReentrantLock playerBarLock) {
		this.playerBarLock = playerBarLock;
		this.color = color;
		this.playPanel = playPanel;
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Rectangle getBarBounds() {
		return new Rectangle(x, y, WIDTH, HEIGHT);
	}

	public void paint(Graphics g) {
		g.setColor(color);
		g.fillRoundRect(x, y, WIDTH, HEIGHT, 10, 10);
	}

	public void keyPressed(int keyCode) {

		playerBarLock.lock();

		// Movimiento a la izquierda
		if (keyCode == MOVE_LEFT) {
			x = canMoveLeft(keyCode) ? x - SPEED_ONE : x;
		}

		// Movimiento hacia arriba
		if (keyCode == MOVE_UP) {
			y = canMoveUp(keyCode) ? y - SPEED_ONE : y;
		}

		// Movimiento hacia abajo
		if (keyCode == MOVE_DOWN) {
			y = canMoveDown(keyCode) ? y + SPEED_ONE : y;
		}

		// Movimiento a la derecha
		if (keyCode == MOVE_RIGHT) {
			x = canMoveRight(keyCode) ? x + SPEED_ONE : x;
		}

		// Movimiento a la izquierda mas rápido
		if (keyCode == (MOVE_LEFT | SPEED_MODIFIER)) {
			x = canMoveLeft(keyCode) ? x - SPEED_TWO : x;
		}

		// Movimiento hacia arriba mas rápido
		if (keyCode == (MOVE_UP | SPEED_MODIFIER)) {
			y = canMoveUp(keyCode) ? y - SPEED_TWO : y;
		}

		// Movimiento hacia abajo más rápido
		if (keyCode == (MOVE_DOWN | SPEED_MODIFIER)) {
			y = canMoveDown(keyCode) ? y + SPEED_TWO : y;
		}

		// Movimiento a la derecha más rápido
		if (keyCode == (MOVE_RIGHT | SPEED_MODIFIER)) {
			x = canMoveRight(keyCode) ? x + SPEED_TWO : x;
		}

		playerBarLock.unlock();

		// La barra no debe de salir del campo de juego
		assert x > 0 && x < playPanel.getWidth() : "Bar was outside field, x: " + x + " y: " + y;

	}

	private boolean canMoveLeft(int keyCode) {

		if (keyCode == MOVE_LEFT) {
			return x - SPEED_ONE > 0
					&& !playPanel.getCpuBar().getBarBounds().intersects(new Rectangle(x - SPEED_ONE, y, WIDTH, HEIGHT));
		}

		if (keyCode == (MOVE_LEFT | SPEED_MODIFIER)) {
			return x - SPEED_TWO > 0
					&& !playPanel.getCpuBar().getBarBounds().intersects(new Rectangle(x - SPEED_TWO, y, WIDTH, HEIGHT));
		}
		return false;
	}

	private boolean canMoveRight(int keyCode) {

		if (keyCode == MOVE_RIGHT) {
			return x + SPEED_ONE < playPanel.getWidth() - 1 - WIDTH
					&& !playPanel.getCpuBar().getBarBounds().intersects(new Rectangle(x + SPEED_ONE, y, WIDTH, HEIGHT));
		}

		if (keyCode == (MOVE_RIGHT | SPEED_MODIFIER)) {
			return x + SPEED_TWO < playPanel.getWidth() - 1 - WIDTH
					&& !playPanel.getCpuBar().getBarBounds().intersects(new Rectangle(x + SPEED_TWO, y, WIDTH, HEIGHT));
		}
		return false;
	}

	private boolean canMoveUp(int keyCode) {

		if (keyCode == MOVE_UP) {
			return y - SPEED_ONE > 0
					&& !playPanel.getCpuBar().getBarBounds().intersects(new Rectangle(x, y - SPEED_ONE, WIDTH, HEIGHT));
		}

		if (keyCode == (MOVE_UP | SPEED_MODIFIER)) {
			return y - SPEED_TWO > 0
					&& !playPanel.getCpuBar().getBarBounds().intersects(new Rectangle(x, y - SPEED_TWO, WIDTH, HEIGHT));
		}
		return false;
	}

	private boolean canMoveDown(int keyCode) {

		if (keyCode == MOVE_DOWN) {
			return y + SPEED_ONE < getMaximumHeight()
					&& !playPanel.getCpuBar().getBarBounds().intersects(new Rectangle(x, y + SPEED_ONE, WIDTH, HEIGHT));
		}

		if (keyCode == (MOVE_DOWN | SPEED_MODIFIER)) {
			return y + SPEED_TWO < getMaximumHeight()
					&& !playPanel.getCpuBar().getBarBounds().intersects(new Rectangle(x, y + SPEED_TWO, WIDTH, HEIGHT));
		}
		return false;
	}

	private int getMaximumHeight() {
		return PlayFrame.HEIGHT - (PlayerBar.HEIGHT * 5 - 3);
	}
}
