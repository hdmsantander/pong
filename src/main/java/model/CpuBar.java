package main.java.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.concurrent.locks.ReentrantLock;

public class CpuBar extends Thread {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1156109103272227517L;

	public static final int WIDTH = 50;
	public static final int HEIGHT = 9;

	private int x;
	private int y;

	private boolean active = true;

	private long globalLagMilis;

	private int globalLagNanos;

	private PlayPanel playPanel;

	private ReentrantLock playerBarLock;

	private ReentrantLock ballLock;

	private Color color;

	private int barSpeed = 1;

	private int barMovementSteps = 1;

	public CpuBar(Color color, PlayPanel playPanel, int x, int y, ReentrantLock playerBarLock, ReentrantLock ballLock) {
		this.playerBarLock = playerBarLock;
		this.ballLock = ballLock;
		this.color = color;
		this.playPanel = playPanel;
		this.x = x;
		this.y = y;
	}

	@Override
	public void run() {
		update();
	}
	
	public void stopRefresh() {
		active = false;
	}

	public void pushTheTempo() {
		barSpeed++;
	}

	public void resetTheTempo() {
		barSpeed = 1;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Rectangle getBarBounds() {
		return new Rectangle(x, y, WIDTH, HEIGHT);
	}

	public void paint(Graphics g) {
		g.setColor(color);
		g.fillRoundRect(x, y, WIDTH, HEIGHT, 10, 10);
	}

	// Método principal para hacer update de la posición de la barra
	private void update() {

		while (active) {
			
			// Para no gastar recursos, refrescamos solo cada cierto tiempo
			waitSomeTime();
			
			// Si la barra del jugador está encima del cpu, hay que hacer algo al respecto
			while (isTheBarBlocked()) {
				
				// Revisamos si esquivar a la izquierda es posible
				boolean dodgeRight = dodgeRight();
				
				// Esquivamos la barra del jugador hacia donde sea posible
				if (dodgeRight) {
					waitSomeTime(globalLagMilis, globalLagNanos);
					moveRight();
				} else {
					waitSomeTime(globalLagMilis, globalLagNanos);
					moveLeft();
				}

			}

			// Revisamos la altura de la barra
			levelBar();
			
			// Seguimos la pelota
			chaseBall();

		}
	}

	private void moveUp() {
		if (y - barMovementSteps > 0 && !playPanel.getPlayerBar().getBarBounds()
				.intersects(new Rectangle(x, y - barMovementSteps, WIDTH, HEIGHT))) {
			y -= barMovementSteps;
		}
	}

	private void moveDown() {
		if (y + barMovementSteps < getMaximumHeight() && !playPanel.getPlayerBar().getBarBounds()
				.intersects(new Rectangle(x, y + barMovementSteps, WIDTH, HEIGHT))) {
			y += barMovementSteps;
		}

		// Evitamos estar muy cerca del jugador 
		while (playPanel.getPlayerBar().getBarBounds()
				.intersects(new Rectangle(x, y + barMovementSteps, WIDTH, HEIGHT + 4))) {
			y += barMovementSteps;
		}
	}

	private void moveLeft() {
		if (x - barMovementSteps > 0 && !playPanel.getPlayerBar().getBarBounds()
				.intersects(new Rectangle(x - barMovementSteps, y, WIDTH, HEIGHT))) {
			x -= barMovementSteps;
		}
	}

	private void moveRight() {
		if (x + barMovementSteps < playPanel.getWidth() - 1 - WIDTH && !playPanel.getPlayerBar().getBarBounds()
				.intersects(new Rectangle(x + barMovementSteps, y, WIDTH, HEIGHT))) {
			x += barMovementSteps;
		}
	}
	
	// Revisa si la barra del CPU está en la sombra de la barra del jugador
	private boolean isTheBarBlocked() {

		playerBarLock.lock();

		int barX = playPanel.getPlayerBar().getX();
		int barY = playPanel.getPlayerBar().getX();

		playerBarLock.unlock();

		return getBarBounds().intersects(new Rectangle(barX, barY, WIDTH + barMovementSteps * 2, getMaximumHeight()));

	}

	// Hace dos movimientos, uno en camino a la bola y si está muy lejos de la bola, hace uno hacia abajo
	private void chaseBall() {
		
		ballLock.lock();

		int ballX = playPanel.getPlayerBall().getX();
		int ballY = playPanel.getPlayerBall().getY();

		ballLock.unlock();

		boolean moveRight = ballX > x + WIDTH / 2;
		boolean thinkThisBetter = Math.sqrt(ballX ^ 2 + ballY ^ 2) < 5;
		for (int i = 0; i < 18; i++) {
			if (thinkThisBetter) {
				waitSomeTime(globalLagMilis, globalLagNanos);
				moveDown();
			} else {
				waitSomeTime(globalLagMilis, globalLagNanos);
				if (moveRight)
					moveRight();
				else
					moveLeft();
			}
			waitSomeTime(globalLagMilis, globalLagNanos);
			if (moveRight)
				moveRight();
			else
				moveLeft();
		}

	}
	
	// Procura estar cerca del jugador, pero no muy cerca de la parte superior de la pantalla
	// Cuando estamos en o más allá del nivel 5, la barra empieza a bloquear a la del CPU
	private void levelBar() {

		int minHeight = PlayFrame.HEIGHT / 4;

		int additionalClearance = barSpeed >= 5 ? HEIGHT + 3 : 0;

		playerBarLock.lock();

		int playerBarY = playPanel.getPlayerBar().getY();

		playerBarLock.unlock();

		if (playerBarY - additionalClearance < y && y > minHeight) {
			while (playerBarY - additionalClearance < y) {
				waitSomeTime(globalLagMilis, globalLagNanos);
				moveUp();
			}
		}

		if (playerBarY > y + 4 * HEIGHT && y < getMaximumHeight() - 1) {
			while (playerBarY > y + 4 * HEIGHT) {
				waitSomeTime(globalLagMilis, globalLagNanos);
				moveDown();
			}
		}

	}
	
	// Revisa si es viable un movimiento a la derecha para esquivar la barra del jugador
	private boolean dodgeRight() {
		playerBarLock.lock();

		int barX = playPanel.getPlayerBar().getX();
		int barY = playPanel.getPlayerBar().getX();

		playerBarLock.unlock();

		Rectangle shadowOfPlayerBar = new Rectangle(barX, barY, WIDTH + barMovementSteps * 2, getMaximumHeight());

		int xIncrements = 1;

		while (x + xIncrements < playPanel.getWidth() - 1) {
			if (!new Rectangle(x + xIncrements, y, WIDTH, HEIGHT).intersects(shadowOfPlayerBar)) {
				return x + WIDTH + xIncrements < playPanel.getWidth() - 1;
			}
			xIncrements++;
		}

		return false;

	}

	// Revisa si es viable un movimiento a la izquierda para esquivar la barra del jugador
	@SuppressWarnings("unused")
	private boolean dodgeLeft() {

		playerBarLock.lock();

		int barX = playPanel.getPlayerBar().getX();
		;
		int barY = playPanel.getPlayerBar().getX();
		;

		playerBarLock.unlock();

		Rectangle shadowOfPlayerBar = new Rectangle(barX, barY, WIDTH + barMovementSteps * 2, getMaximumHeight());

		int xIncrements = -1;

		while (x + xIncrements > 0) {
			if (!new Rectangle(x + xIncrements, y, WIDTH, HEIGHT).intersects(shadowOfPlayerBar)) {
				return true;
			}
			xIncrements--;
		}

		return false;
	}

	private void waitSomeTime() {

		long waitingTimeMilis = -1;
		int waitingTimeNanos = -1;
		
		barSpeed = barSpeed > 10 ? 10:barSpeed;

		switch (barSpeed) {
		case 1:
			waitingTimeMilis = 100;
			waitingTimeNanos = 0;
			globalLagMilis = 5;
			globalLagNanos = 0;
			break;
		case 2:
			waitingTimeMilis = 90;
			waitingTimeNanos = 500000;
			globalLagMilis = 4;
			globalLagNanos = 750000;
			break;
		case 3:
			waitingTimeMilis = 90;
			waitingTimeNanos = 0;
			globalLagMilis = 4;
			globalLagNanos = 500000;
			break;
		case 4:
			waitingTimeMilis = 80;
			waitingTimeNanos = 500000;
			globalLagMilis = 4;
			globalLagNanos = 250000;
			break;
		case 5:
			waitingTimeMilis = 80;
			waitingTimeNanos = 0;
			globalLagMilis = 4;
			globalLagNanos = 0;
			break;
		case 6:
			waitingTimeMilis = 70;
			waitingTimeNanos = 500000;
			globalLagMilis = 3;
			globalLagNanos = 750000;
			break;
		case 7:
			waitingTimeMilis = 70;
			waitingTimeNanos = 0;
			globalLagMilis = 3;
			globalLagNanos = 500000;
			break;
		case 8:
			waitingTimeMilis = 60;
			waitingTimeNanos = 0;
			globalLagMilis = 3;
			globalLagNanos = 250000;
			break;
		case 9:
			waitingTimeMilis = 50;
			waitingTimeNanos = 0;
			globalLagMilis = 3;
			globalLagNanos = 0;
			break;
		case 10:
			waitingTimeMilis = 40;
			waitingTimeNanos = 0;
			globalLagMilis = 2;
			globalLagNanos = 750000;
			break;
		default:
			waitingTimeMilis = 30;
			waitingTimeNanos = 0;
			globalLagMilis = 2;
			globalLagNanos = 500000;
		}

		try {
			Thread.sleep(waitingTimeMilis, waitingTimeNanos);
		} catch (InterruptedException e) {
			throw new RuntimeException("Error sleeping bar refresh thread", e);
		}
	}

	private void waitSomeTime(long milis, int nanos) {
		try {
			Thread.sleep(milis, nanos);
		} catch (InterruptedException e) {
			throw new RuntimeException("Error sleeping bar refresh thread", e);
		}
	}

	private int getMaximumHeight() {
		return PlayFrame.HEIGHT - (HEIGHT * 5 - 3);
	}

}